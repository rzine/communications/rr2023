# Présentation du projet Rzine  [<img src="figure/Rzine.png"  align="right" width="120"/>](http://rzine.fr/)
### Pour la diffusion et le partage de ressources sur la pratique de R en sciences humaines et sociales 

### Communication aux rencontres R 2023

[Voir les slides](https://rzine.gitpages.huma-num.fr/communications/rr2023/)

